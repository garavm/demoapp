import React, { Component } from 'react';
import { StyleSheet, Text, View, Dimensions, TouchableOpacity } from 'react-native';
import DeviceInfo from "react-native-device-info";
const deviceId = DeviceInfo.getDeviceId();
import MapView from 'react-native-maps';

import firebase from 'react-native-firebase';


export default class App extends Component {

  componentDidMount() {
    // this._allowNotification();
    firebase.analytics().setCurrentScreen('home_screen');
    firebase.analytics().setUserProperty('userType', 'developer');
    firebase.analytics().setUserProperty('clientId', DeviceInfo.getUniqueID());
    firebase.analytics().setUserProperty('version', DeviceInfo.getReadableVersion());
    firebase.analytics().setUserProperty('bundleid', DeviceInfo.getBundleId());
    firebase.analytics().setUserProperty('carrier', DeviceInfo.getCarrier());

    // firebase.analytics().setUserProperty('userName', 'Gaurav');
    
    firebase.analytics().setAnalyticsCollectionEnabled(true);
    // firebase.analytics().setUserId('djnvfdj');
  }



  
  _allowNotification = async () => {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      // user has permissions
      alert('ffvfd')
    } else {
      // user doesn't have permission
      alert('dgcvdg')
    }
  }

  _analytics = () => {

  }
  _notification = () => {
    firebase.analytics().logEvent("buttonPressed");
    // firebase.messaging().createLocalNotification({
    //   title: 'Example Title',
    //   body: 'Example notification body with longer text',
    //   show_in_foreground: true,
    //   sound: false,
    // });
  }

  render() {
    const { region } = this.props;
    console.log(region);
    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <MapView
            style={styles.map}
            region={{
              latitude: 37.78825,
              longitude: -122.4324,
              latitudeDelta: 0.015,
              longitudeDelta: 0.0121,
            }}
          >
          </MapView>
        </View>
        <View style={{ marginTop: Dimensions.get("window").height * 0.55, flex: 1, justifyContent: 'flex-start' }}>
          <Text>{deviceId}</Text>
          <TouchableOpacity onPress={this._notification} style={{ backgroundColor: 'orange', justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ padding: 10 }}>Press to get notification</Text>
          </TouchableOpacity>
            {firebase.admob.nativeModuleExists && <Text style={styles.module}>admob()</Text>}
            {firebase.analytics.nativeModuleExists && <Text style={styles.module}>analytics()</Text>}
            {firebase.auth.nativeModuleExists && <Text style={styles.module}>auth()</Text>}
            {firebase.config.nativeModuleExists && <Text style={styles.module}>config()</Text>}
            {firebase.crashlytics.nativeModuleExists && <Text style={styles.module}>crashlytics()</Text>}
            {firebase.database.nativeModuleExists && <Text style={styles.module}>database()</Text>}
            {firebase.firestore.nativeModuleExists && <Text style={styles.module}>firestore()</Text>}
            {firebase.functions.nativeModuleExists && <Text style={styles.module}>functions()</Text>}
            {firebase.iid.nativeModuleExists && <Text style={styles.module}>iid()</Text>}
            {firebase.invites.nativeModuleExists && <Text style={styles.module}>invites()</Text>}
            {firebase.links.nativeModuleExists && <Text style={styles.module}>links()</Text>}
            {firebase.messaging.nativeModuleExists && <Text style={styles.module}>messaging()</Text>}
            {firebase.notifications.nativeModuleExists && <Text style={styles.module}>notifications()</Text>}
            {firebase.perf.nativeModuleExists && <Text style={styles.module}>perf()</Text>}
            {firebase.storage.nativeModuleExists && <Text style={styles.module}>storage()</Text>}
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  content: {
    marginTop: 20,
    ...StyleSheet.absoluteFillObject,
    height: Dimensions.get("window").height * 0.5,
    width: 400,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});
